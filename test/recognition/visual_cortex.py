import keras
import sys

from keras.callbacks import ModelCheckpoint, EarlyStopping

from test import visualizer


class VisualCortext:
    def __init__(self, train_x, test_x, train_y, test_y):
        self.train_x = train_x / 255.
        self.test_x = test_x / 255.
        # rid of magic number
        self.train_y = keras.utils.to_categorical(train_y, 2)
        self.test_y = keras.utils.to_categorical(test_y, 2)

    def classify(self, model, save_path):
        checkpoint = ModelCheckpoint(save_path, monitor='val_acc', verbose=1, save_best_only=True, mode='max')

        model.compile(loss=keras.losses.categorical_crossentropy,
                      optimizer='adam',
                      metrics=['accuracy'])

        if '--load' in sys.argv:
            model.load_weights(save_path)

        # visualizer.training_history_visualization(model.fit(self.train_x, self.train_y,
        #           batch_size=8,
        #           epochs=50,
        #           verbose=1,
        #           validation_data=(self.test_x, self.test_y),
        #           callbacks=[checkpoint]))
        score = model.evaluate(self.test_x, self.test_y, verbose=0)
        print("Test loss: ", score[0])
        print("Test accuracy: ", score[1])
