import numpy as np
from keras.preprocessing import image
from os import listdir
from os.path import isfile, join


class MriStream:
    def __init__(self, folder_path, count=-1) -> None:
        super().__init__()
        self.folder_path = folder_path
        self.count = folder_path

    def __look_dir__(self):
        return [self.folder_path + '/' + blob for blob in listdir(self.folder_path) if
                isfile(join(self.folder_path, blob))]

    def load(self):
        self.files = self.__look_dir__()
        self.count = len(self.files)
        # need to customize target size
        self.img_data = [image.load_img(img, target_size=(128, 128)) for img in self.files]
        return self

    def map(self):
        if self.img_data is None:
            return
        self.array = np.array([image.img_to_array(img) for img in self.img_data])
        return self

    def collect(self):
        return self.array, np.array([1 if s.replace(".png", "").endswith("t") else 0 for s in self.files])
