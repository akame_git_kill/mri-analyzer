import numpy as np
import matplotlib.pyplot as plt


def plot(img, trace=False, y=None):
    if trace is True:
        print(trace)
    plt.imshow(img / 255.)
    plt.show()
    if y is not None:
        print("y:" + str(y))


def have_look(datagen, img, path):
    img = img.reshape((1,) + img.shape)
    i = 0
    for _ in datagen.flow(img, batch_size=1,save_to_dir=path, save_prefix='gen', save_format='png'):
        i += 1
        if i > 20: return
