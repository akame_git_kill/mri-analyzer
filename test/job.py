from keras.preprocessing.image import ImageDataGenerator

from test import ulimit, visualizer
from test.generator import Generator
from test.recognition import keras_factory, utils
from test.recognition.mri_stream import MriStream
from test.recognition.visual_cortex import VisualCortext

path = '/home/salieri/Desktop/dataset'
test_path = '/home/salieri/Desktop/dataset_test'
preview_path = '/home/salieri/Desktop/preview'
save_path = '/home/salieri/Desktop/saved/weights.best.0975.hdf5'


def main():
    train_x, train_y = MriStream(path).load().map().collect()
    test_x, test_y = MriStream(test_path).load().map().collect()

    print(train_x.shape)
    print(train_y.shape)

    print(test_x.shape)
    print(test_y.shape)

    cortex = VisualCortext(train_x, test_x, train_y, test_y)
    cortex.classify(keras_factory.keras_seq_factory(2, input_shape=(128, 128, 3)), save_path)


if __name__ == '__main__':
    ulimit.run()
    main()